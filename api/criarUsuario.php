<?php
require '../classes/Usuario.php';

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json;charset=utf-8") {

    $content = trim(file_get_contents("php://input"));

    $dados = json_decode($content, true);

    $usuario = new Usuario($dados["email"], $dados["senha"]);
    $mensagem = $usuario->adcionarUsuario();
    $retorno = array("mensagem"=>$mensagem);
    echo json_encode($retorno);

}
