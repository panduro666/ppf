<?php
class Conexao
{
    private $mysqli;

    function __construct()
    {
        $this->conecta();
    }

    public function conecta()
    {
        $hostname = "localhost";
        $bancodedados = "projetinho";
        $usuario = "root";
        $senha = "";

        $this->mysqli = new mysqli($hostname, $usuario, $senha, $bancodedados);

        if ($this->mysqli->connect_errno) {
            echo "Falha ao conectar: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
        }
    }

    public function executarQuery($sql)
    {
        if ($this->mysqli->query($sql) === TRUE) {
            return true;
        } else {
            return false;
        }
    }
}
