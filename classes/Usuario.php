<?php
require 'Conexao.php';

class Usuario
{
    public $email;
    public $senha;

    function __construct($email, $senha){
        $this->setSenha($senha);
        $this->setEmail($email);
    }

    function adcionarUsuario(){
        $conexao = new Conexao;
        $status = $conexao->executarQuery("INSERT INTO `usuarios`(`email`, `senha`) VALUES ('".$this->email."','".$this->senha."')");
        if($status){
            return "Usuario criado";
        }else{ 
            return "Erro ocorrido ao tentar adicionar o usuario";
        }
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
